var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglifyjs'),
    concat = require('gulp-concat'),
    browserSync = require('browser-sync').create();


gulp.task('sass', function () {
    return gulp.src('dist/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('app/css/'))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function () {
    return gulp.src([
        'dist/js/*.js'
    ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'))
        .pipe(browserSync.stream());
});

gulp.task('watch', ['sass', 'scripts'], function () {
    browserSync.init({
        server: "app"
    });
    gulp.watch('dist/sass/**/*.scss', ['sass']);
    gulp.watch('dist/js/*.js', ['scripts']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});

gulp.task('default', ['watch']);