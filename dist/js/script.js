(function ($) {
    var topBlock = $('.curriculum-block'),
        botBlock = $('.examples-block'),
        mainBlock = $('.main-block'),
        btnTop = $('.btn-top'),
        btnBot = $('.btn-bot');

    //MAIN BTN TOP
    $('[data-top]').click(function () {
        $(window).scrollTop(0);
        mainBlock.animate({
            'top': '100vh'
        }, 800 ,function () {
            mainBlock.css('display','none');
        });
    });

    //MAIN BTN BOT
    $('[data-bot]').click(function () {
        topBlock.css('display','none');
        mainBlock.animate({
            'top': '-100vh'
        }, 800 ,function () {
            mainBlock.css('display','none')
        });
        botBlock.animate({
            'top': '0px'
        },800,function () {
            botBlock.css('position','relative')
        })
    });

    //ALL BTN BOT
    btnBot.click(function () {
        botBlock.animate({
            'top':'0px'
        },800,function () {
            topBlock.css('display','none');
            $(window).scrollTop(0);
            botBlock.css('position','relative');
        })
    });

    //ALL BTN TOP
    btnTop.click(function () {
        botBlock.css('position','fixed');
        topBlock.css('display','block');
        botBlock.animate({
            'top':'100vh'
        },800)
    });

    $(window).load(function () {
        setTimeout(function () {
            mainBlock.addClass('finish');
        },500);

    });
















    // var btnCurriculum = $('.btn-curriculum');
    //
    // var curriculumHeight = $('.curriculum-block>.container').height();
    // var mainBlock = ;
    // var btnExample = $('.btn-example');
    // var example = $('.examples-block');
    // btnCurriculum.click(function () {
    //     // curriculum.animate({
    //     //     'height': curriculumHeight + 'px'
    //     // }, 1500, function () {
    //     //     mainBlock.addClass('die');
    //     //     example.css({
    //     //         'height': '0px',
    //     //         'top': '100vh'
    //     //     })
    //     // });
    //     example.css({
    //         'position': 'fixed',
    //         'top': '0'
    //     });
    //     curriculum.css('display','block');
    //
    //
    // });
    //
    //
    // btnExample.click(function () {
    //     example.css({
    //         'height': 'auto',
    //         'position': 'fixed',
    //         'top': '100vh'
    //     });
    //     mainBlock.animate({
    //         'top': '-100vh'
    //     }, 1000);
    //     example.animate({
    //         'top': '0',
    //         'z-index': '2'
    //     }, 1000, function () {
    //         mainBlock.addClass('die');
    //         curriculum.css('display','none');
    //         setTimeout(function () {
    //             $('body').scrollTop(0);
    //             example.css('position', 'relative')
    //         }, 10);
    //     })
    //
    //
    // });
})(jQuery);
